/*
  Text fields
*/
import TextField from 'material-ui/lib/text-field';
import React from 'react';

// The fields
export default function OneLineTextField(data) {
  return (
    <div>
      <span style={{ display: 'inline-block', width: '9em' }}>
        { data.description }:
      </span>
      {' '}
      <TextField type="number" min="1" {...data} required />
      {' '}
      { data.postDescription }
    </div>
  );
}
