/*
  Chart of clusters
*/
import React from 'react';
import Chart from 'chart.js';
import Card from 'material-ui/lib/card/card';
import CardTitle from 'material-ui/lib/card/card-title';
import Legend from './legend.js';
import Cluster from './cluster.js';

Chart.defaults.global = {
  ...Chart.defaults.global,
  multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>"
};

// For Performance (we should not render the graph for each state change)
export default class ClustersPerformance extends React.Component {
  shouldComponentUpdate(nextProps) {
    if (JSON.stringify(nextProps) !== JSON.stringify(this.props)) {
      return true;
    }

    return false;
  }
  render() {
    return <Clusters {...this.props} />;
  }
}

function Clusters({ openDialog, result, features }) {
  var clusters = result.map((cluster, i) => (
    <Cluster openDialog={ openDialog } cluster={ cluster } key={ i } id={ i } features={ features }/>
  ));

  return (
    <Card>
      <CardTitle
        title="Stock Analysis Results"
        subtitle="Tom Boldan & Gal Schlezinger"
      />
      <Legend />
      <div style={{ display: "flex" }}>
        { clusters }
      </div>
    </Card>
  );
}
