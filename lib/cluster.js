/*
  Cluster
*/
import React from 'react';
import CardHeader from 'material-ui/lib/card/card-header';
import Stock from './stock.js';

export default function Cluster({ openDialog, cluster, id, features }) {
  return (
    <div>
      <CardHeader
        title={ `Cluster #${id+1}` }
        subtitle={ cluster.length }
      />
      { cluster.map(stock => <Stock { ...stock } openDialog={ openDialog } key={ stock.symbol } features={ features }/>) }
  </div>
);
}
