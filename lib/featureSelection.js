/*
  Home page
*/
import React from 'react';
import Card from 'material-ui/lib/card/card';
import CardActions from 'material-ui/lib/card/card-actions';
import CardTitle from 'material-ui/lib/card/card-title';
import RaisedButton from 'material-ui/lib/raised-button';
import TextField from 'material-ui/lib/text-field';
import CircularProgress from 'material-ui/lib/circular-progress';
import OneLineToggle from './oneLineToggle.js';
import OneLineTextField from './oneLineTextField.js';

export default class FeatureSelection extends React.Component {
  // Start clusering
  fetchData(event) {
    event.preventDefault();
    this.setState({ loading: true });

    var isOpen = this.refs.open.isToggled();
    var isClose = this.refs.close.isToggled();
    var isLow = this.refs.low.isToggled();
    var isHigh = this.refs.high.isToggled();

    var url = `/start?stocks=${this.state.stocks}&days=${this.state.days}&clusters=${this.state.clusters}` +
              `&open=${isOpen}&close=${isClose}` +
              `&high=${isHigh}&low=${isLow}`;

    fetch(url).then(
      data => data.json()
    ).then(
      this.props.onReceivedData.bind(null, { open: isOpen, close: isClose, low: isLow, high: isHigh })
  )}

  onChange(stateName){
    return e => {
      this.setState({
        [stateName]: e.target.value
      });
    }
  }

  constructor(props) {
    super(props);
    this.state = { loading: false }
  }

  render() {
    var { onReceivedData } = this.props;

    return (
    <form onSubmit={ this.fetchData.bind(this) }>
      <Card>
        <CardTitle
          title="Stock Analysis using Hadoop"
          subtitle="Tom Boldan & Gal Schlezinger"
        />
        <div style={{ display: 'flex' }}>
        <div style={{ width: '700px' }}>
        <CardActions>
        <div
          style={{ display: 'flex' }}
        >
          <div>
            <OneLineTextField
              onChange={ this.onChange("stocks") }
              description="Number of Stocks"
              hintText="100"
            />
            <OneLineTextField
              onChange={ this.onChange("days") }
              description="Days to analyze"
              postDescription="days from today backwards"
              hintText="30"
            />
            <OneLineTextField
              onChange={ this.onChange("clusters") }
              description="Number of clusters"
              hintText="7"
            />

            <div
              style={{
                lineHeight: 3,
              }}
            >
              Features to Analyze
            </div>

            <OneLineToggle
              label="Open"
              ref="open"
            />
            <OneLineToggle
              label="Close"
              ref="close"
            />
            <OneLineToggle
              label="High"
              ref="high"
            />
            <OneLineToggle
              label="Low"
              ref="low"
            />
          </div>
        </div>

        <RaisedButton
          disabled={ this.state.loading }
          type="Submit"
          style={{ marginTop: '0.5em' }}
          primary={ true }
          label="Analyze!"
        />
        </CardActions>
        </div>
          {
            this.state.loading &&
            <div style={{ display: 'flex', flex: 1, justifyContent: 'center', }}>
              <CircularProgress mode="indeterminate" size={2} />
            </div> }
      </div>
      </Card>
    </form>
    );
  }
}
