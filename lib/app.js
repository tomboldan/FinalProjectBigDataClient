/*
  The application container
*/
import React from 'react';
import FeatureSelection from './featureSelection.js';
import Chart from './chart.js';
import AppBar from 'material-ui/lib/app-bar';
import Dialog from 'material-ui/lib/dialog';
import FlatButton from 'material-ui/lib/flat-button';
import IconButton from 'material-ui/lib/icon-button';
import ActionHome from 'material-ui/lib/svg-icons/action/home';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      page: '',
      dialogContent: null,
      result: [], // The clusering result
      features: {},
    };
  }

  changeToChart(features, data) {
    this.setState({ page: 'chart', result: data, features: features });
  }

  changeToHome() {
    this.setState({ page: '' });
  }

  page()
  {
    switch (this.state.page) {
      case 'chart': // Cluster results
        return <Chart openDialog={ this.openDialog.bind(this) } result={ this.state.result } features={ this.state.features }/>;
        break;
      case 'loading': // Loading circular progress
        return <CircularProgress mode="indeterminate" size={ 2 } />;
      default: // Home page
        return <FeatureSelection onReceivedData={ this.changeToChart.bind(this) } />;
        break;
    }
  }

  closeDialog(){
    this.setState({ dialogContent: null });
}

openDialog(data){
  this.setState({ dialogContent: data })
}

  render() {
    // The dialog buttons
    const actions = [
          <FlatButton
            label="Close"
            primary={ true }
            keyboardFocused={ true }
            onClick={ this.closeDialog.bind(this) } />,
        ];

// The dialog content
  var content = this.state.dialogContent;

    return (
      <div>
        <AppBar
          title="Stock Analysis"
          iconElementLeft={
            <IconButton onClick={ this.changeToHome.bind(this) } >
              <ActionHome color="white" />
            </IconButton> }
        />
        <Dialog
          title={ content && content.title }
          actions={ actions }
          modal={ false }
          open={ content != null }
          onRequestClose={ this.closeDialog.bind(this) }>
          { content && content.data }
        </Dialog>
        { this.page() }
      </div>
    )
  }
}
