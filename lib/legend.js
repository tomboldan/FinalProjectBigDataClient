/*
  Legend
*/
import React from 'react';
import FloatingActionButton from 'material-ui/lib/floating-action-button';
import { OPEN_COLOR, CLOSE_COLOR, HIGH_COLOR, LOW_COLOR } from './theme.js';

function FeatureToggle({ color, text, onClick }) {
  return (
    <FloatingActionButton backgroundColor={ color } iconStyle={{ color: "white" }} style={{ marginRight: 10 }}>
            { text }
    </FloatingActionButton>
);
}

export default function Legend()
{
  return (
    <div style={{ marginLeft: 10 }}>
      <FeatureToggle text="Open" color={ OPEN_COLOR } />
      <FeatureToggle text="Close" color={ CLOSE_COLOR } />
      <FeatureToggle text="High" color={ HIGH_COLOR } />
      <FeatureToggle text="Low" color={ LOW_COLOR } />
    </div>
  );
}
