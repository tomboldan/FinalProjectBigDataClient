/*
  Stock graph
*/
import React from 'react';
import { OPEN_COLOR, CLOSE_COLOR, HIGH_COLOR, LOW_COLOR } from './theme.js';
import { Line as LineChart } from 'react-chartjs';

export default function StockChart(stock) {
    var data = createChartData(stock);
    if (!stock.expanded)
    {
        return (
          <LineChart
            data={ data }
            options={{
              scaleShowLabels: false,
              showScale: false,
              pointDot: false,
              showTooltips: false,
              animation:false }}
            width="190" height="50"
          />
      );
    }

    return (
      <LineChart
        data={ data }
        width="700"
        height="300"
        options={{
        animation:false,
        }}
    />
  );
}

function createChartData(stock) {
  var graph = [{
    data: stock.open,
    feature: 'Open',
    color: OPEN_COLOR,
  }, {
    data: stock.close,
    feature: 'Close',
    color: CLOSE_COLOR,
  }, {
    data: stock.high,
    feature: 'High',
    color: HIGH_COLOR,
  }, {
    data: stock.low,
    feature: 'Low',
    color: LOW_COLOR,
  }].filter(x => stock.features[x.feature.toLowerCase()]).map(createFeature);

  var data = {
    labels: stock.date,
    datasets: graph,
  };

  return data;
}

function createFeature({ feature, data, color }) {
  return {
            label: feature,
            fillColor: "rgba(220,220,220,0.1)",
            strokeColor: color,
            pointColor:  color,
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(220,220,220,1)",
            data
        };
}
