/*
  Starts the application
*/
import ReactDOM from 'react-dom';
import React from 'react';
import App from './app.js';
import MyRawTheme from './theme.js';
import ThemeManager from 'material-ui/lib/styles/theme-manager';
import ThemeDecorator from 'material-ui/lib/styles/theme-decorator';

// Put the applications' themes
const AppThemer = ThemeDecorator(ThemeManager.getMuiTheme(MyRawTheme))(App);

// Render the theme app
ReactDOM.render(<AppThemer />, document.getElementById('main'));
window.perf = require('react-addons-perf');
