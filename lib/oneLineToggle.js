/*
  Checkbox toggle
*/
import React from 'react';
import Checkbox from 'material-ui/lib/checkbox';

export default class OneLineToggle extends React.Component {
  isToggled(){
    return this.refs.check.isChecked();
  }

render() {
// Get the data from the parent
  var data = this.props;

  return (
      <Checkbox
        {...data}
        ref="check"
        name={ data.label.toLowerCase() }
        label={ data.label }
        value="true"
        style={{ float: 'left', width: 'auto', marginRight: 30 }}
        defaultChecked={ true }
      />
  );
}
}
