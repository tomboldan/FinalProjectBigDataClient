/*
  Stock
*/
import React from 'react';
import StockChart from './stockChart';
import FlatButton from 'material-ui/lib/flat-button';

export default function Stock(stock) {
  var onClick = () => stock.openDialog({
                        data: StockChart({ ...stock, expanded: true }),
                        title: stock.symbol
                      });
  return (
    <div>
      <FlatButton label={ stock.symbol } onClick={ onClick } />
      <div>
        { StockChart({ ...stock, expanded: false }) }
      </div>
    </div>
        )
}
