Final BigData Project Client
============================
Stock analysis by @tomboldan and @Schniz

Environment Variables
---------------------

- `FINAL_BIGDATA_USER` - the username for the Cloudera host
- `FINAL_BIGDATA_PASSWORD` - the password for the Cloudera host
- `FINAL_BIGDATA_HOST` - the hostname or ip for the Cloudera host

Installing
----------

- Clone the project
- Import it on Eclipse, and use Maven to download dependencies.
- Run `webpack` on the command line for compiling the JavaScript sources and creating a distributable JS file.
- Run the `Main.java` class to start the server locally
- go to http://localhost:4567 and provide the data to create the clustering.

That's it :princess::boy:
