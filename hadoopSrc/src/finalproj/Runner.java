package finalproj;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;

import finalproj.mapreduce.CanopyMapper;
import finalproj.mapreduce.CanopyReducer;
import finalproj.mapreduce.CentersMapper;
import finalproj.mapreduce.CentersReducer;
import finalproj.mapreduce.KMeansMapper;
import finalproj.mapreduce.KMeansReducer;
import finalproj.models.Center;
import finalproj.models.CenterWithCanopy;
import finalproj.models.Stock;

/**
 * Main executer
 * 
 * @author Tom Boldan and Gal Schlezinger
 * 
 */
public class Runner {
	private static final Log LOG = LogFactory.getLog(Runner.class);

	/**
	 * Main
	 * 
	 * @param args
	 *            [base, k]
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws IOException,
			InterruptedException, ClassNotFoundException {
		Globals.setPath(args[0], Integer.parseInt(args[1]));

		Job canopyJob = canopy();
		canopyJob.waitForCompletion(true);

		Job centersJob = centers();
		centersJob.waitForCompletion(true);

		long counter = centersJob.getCounters()
				.findCounter(CentersReducer.Counter.CANOPIES).getValue();
		LOG.info(counter + " canopies");

		int iterations = kmeans();
		LOG.info("Finished after " + iterations + ".");
	}

	/**
	 * Find canopy centers
	 * 
	 * @return {@link Job}
	 * @throws IOException
	 */
	public static Job canopy() throws IOException {
		Job job = new Job();
		job.setJobName("Canopy");

		job.setJarByClass(Runner.class);

		job.setMapperClass(CanopyMapper.class);
		job.setReducerClass(CanopyReducer.class);

		// Mapper output
		job.setMapOutputKeyClass(IntWritable.class);
		job.setMapOutputValueClass(Center.class);

		// Output
		FileSystem fs = FileSystem.get(job.getConfiguration());

		Path outputPath = new Path(Globals.OUTPUT_CANOPY_ID_PATH);
		if (fs.exists(outputPath)) {
			fs.delete(outputPath, true);
		}

		SequenceFileOutputFormat.setOutputPath(job, outputPath);
		job.setOutputFormatClass(SequenceFileOutputFormat.class);
		job.setOutputKeyClass(IntWritable.class);
		job.setOutputValueClass(Center.class);

		FileInputFormat.addInputPath(job, new Path(Globals.INPUT_VECTORS_PATH));

		return job;
	}

	/**
	 * Find the first kmeans centers for each canopy
	 * 
	 * @return {@link Job}
	 * @throws IOException
	 */
	public static Job centers() throws IOException {
		Job job = new Job();

		job.setJarByClass(Runner.class);
		job.setJobName("Centers");

		job.setMapperClass(CentersMapper.class);
		job.setReducerClass(CentersReducer.class);

		// Output
		FileSystem fs = FileSystem.get(job.getConfiguration());

		Path outputPath = new Path(Globals.OUTPUT_CENTERS_PATH);
		if (fs.exists(outputPath)) {
			fs.delete(outputPath, true);
		}

		SequenceFileOutputFormat.setOutputPath(job, outputPath);
		job.setOutputFormatClass(SequenceFileOutputFormat.class);

		job.setOutputKeyClass(Center.class);
		job.setOutputValueClass(Center.class);

		// Mapper output
		job.setMapOutputKeyClass(Center.class);
		job.setMapOutputValueClass(Stock.class);

		FileInputFormat.addInputPath(job, new Path(Globals.INPUT_VECTORS_PATH));

		Configuration configuration = job.getConfiguration();
		Path center = new Path(Globals.OUTPUT_CANOPY_ID_PATH
				+ Globals.OUTPUT_FILE_NAME);
		configuration.set("canopies.path", center.toString());
		configuration.setInt("ksize", Globals.KSIZE);

		return job;
	}

	/**
	 * Kmeans clustering
	 * 
	 * @return
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws ClassNotFoundException
	 */
	public static int kmeans() throws IOException, InterruptedException,
			ClassNotFoundException {
		int iteration = 1;
		long counter = Globals.KSIZE;

		// Run until there is no more converged!
		while (counter > 0) {
			Path center = new Path(Globals.OUTPUT_CENTERS_PATH
					+ Globals.OUTPUT_FILE_NAME);
			Path outputPath = new Path(Globals.OUTPUT_KMEANS_PATH);

			Configuration configuration = new Configuration();
			configuration.set("centroid.path", center.toString());
			configuration.set("num.iteration", iteration + "");

			Job job = new Job(configuration);
			job.setJobName("KMeans Clustering " + iteration);

			job.setMapperClass(KMeansMapper.class);
			job.setReducerClass(KMeansReducer.class);
			job.setJarByClass(KMeansMapper.class);

			Path in = new Path(Globals.INPUT_VECTORS_PATH);

			FileInputFormat.addInputPath(job, in);
			FileSystem fs = FileSystem.get(configuration);
			if (fs.exists(outputPath)) {
				fs.delete(outputPath, true);
			}
			FileOutputFormat.setOutputPath(job, outputPath);

			job.setOutputKeyClass(Text.class);
			job.setOutputValueClass(Text.class);

			// Mapper output
			job.setMapOutputKeyClass(CenterWithCanopy.class);
			job.setMapOutputValueClass(Stock.class);

			job.waitForCompletion(true);
			iteration++;
			counter = job.getCounters()
					.findCounter(KMeansReducer.Counter.CONVERGED).getValue();
		}

		return iteration;
	}
}
