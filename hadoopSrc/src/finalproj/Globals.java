package finalproj;

/**
 * Globals
 * 
 * @author Tom Boldan and Gal Schlezinger
 * 
 */
public class Globals {
	// Defaults
	public static String INPUT_VECTORS_PATH = "/tmp/f4b36e6b-8e25-4119-83fe-0e3d898b58b1/data";
	public static String OUTPUT_CANOPY_ID_PATH = "/home/training/Desktop/output5/canopyID/";
	public static String OUTPUT_CENTERS_PATH = "/home/training/Desktop/output5/centres/";
	public static String OUTPUT_KMEANS_PATH = "/home/training/Desktop/output5/kmeans/";
	public static String OUTPUT_FILE_NAME = "part-r-00000";

	public static int KSIZE = 7;
	public static int T1 = 750; // for 30 days, 7 k and 100 stocks
	public static int T2 = T1 / 2;

	public static int STOCKS_NUMBER = 0;
	public static int CANOPY_COUNTER = 0;

	/**
	 * Builds the path input and output
	 * 
	 * @param base
	 *            root path
	 * @param clusters
	 *            the number of clusters
	 */
	public static void setPath(String base, int clusters) {
		KSIZE = clusters;
		INPUT_VECTORS_PATH = base + "/data";
		OUTPUT_CANOPY_ID_PATH = base + "/output/canopyID/";
		OUTPUT_CENTERS_PATH = base + "/output/centers/";
		OUTPUT_KMEANS_PATH = base + "/output/kmeans/";
	}
}
