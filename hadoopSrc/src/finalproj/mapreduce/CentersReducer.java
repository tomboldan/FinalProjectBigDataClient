package finalproj.mapreduce;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.mapreduce.Reducer;

import finalproj.Globals;
import finalproj.models.Center;
import finalproj.models.Stock;

/**
 * Finds the center in linear ratio to the number of stocks in the canopy
 * divided by the numbers of stocks (in every canopy) times the number of
 * clusters we want to get.
 * 
 * <br>
 * Reducer &lt;canopy, stock, canopy, center&gt;
 * 
 * @author Tom Boldan and Gal Schlezinger
 * 
 */
public class CentersReducer extends Reducer<Center, Stock, Center, Center> {

	int ksize;

	public static enum Counter {
		/**
		 * For the number of centers received from the canopies.
		 */
		CENTERS, CANOPIES
	}

	protected void setup(Context context) throws IOException,
			InterruptedException {
		Configuration conf = context.getConfiguration();
		ksize = conf.getInt("ksize", Globals.KSIZE);
	}

	@Override
	protected void reduce(Center key, Iterable<Stock> values, Context context)
			throws IOException, InterruptedException {
		context.getCounter(Counter.CANOPIES).increment(1);

		int numberOfCenters = (int) context.getCounter(Counter.CENTERS)
				.getValue();

		// If we found the k centers as we wanted
		if (numberOfCenters >= ksize) {
			return;
		}

		List<Stock> stocks = new ArrayList<Stock>();
		int counter = 0;

		// Counter the number of stocks in canopy
		for (Stock stock : values) {
			counter++;
			stocks.add(new Stock(stock));
		}

		int numberOfStocks = Globals.STOCKS_NUMBER;

		// The linear calculation of how much centers to take from this canopy.
		int currSize = (int) Math
				.ceil((((double) counter / numberOfStocks) * ksize));

		// In a case of size < 1
		currSize = Math.max(currSize, 1);

		// To make sure we won't exceed the ksize limitation.
		currSize = Math.min(currSize, ksize - (int) numberOfCenters);

		context.getCounter(Counter.CENTERS).increment(currSize);

		for (Stock currVal : stocks) {
			// No need for more vectors for this canopy.
			if (currSize == 0) {
				break;
			}

			currSize--;

			Stock roundStock = currVal.round();
			roundStock.setName("");
			context.write(new Center(key), new Center(roundStock));
		}
	}
}
