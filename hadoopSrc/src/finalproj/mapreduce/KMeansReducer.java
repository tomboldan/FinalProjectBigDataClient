package finalproj.mapreduce;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import finalproj.models.Center;
import finalproj.models.CenterWithCanopy;
import finalproj.models.Stock;

/**
 * This class finds the new centers and counts how many centers converged, by
 * the KMeans algorithm.
 * 
 * <br>
 * Reducer &lt;center with canopy, stock, canopy uuid, symbol&gt;
 * 
 * @author Tom Boldan and Gal Schlezinger
 * 
 */
public class KMeansReducer extends Reducer<CenterWithCanopy, Stock, Text, Text> {
	List<CenterWithCanopy> centers = new LinkedList<CenterWithCanopy>();
	Map<String, List<String>> centersToStocks = new HashMap<String, List<String>>();

	public static enum Counter {
		/**
		 * for the number of converged values
		 */
		CONVERGED
	}

	@Override
	protected void reduce(CenterWithCanopy key, Iterable<Stock> values,
			Context context) throws IOException, InterruptedException {
		int amountOfDays = key.getCanopyVector().getDays().size();
		int sizeOfVectors = key.getCanopyVector().getDays().get(0).getSize();
		int counter = 0;

		Stock result = Stock.createDefaultStock(amountOfDays, sizeOfVectors);
		List<Stock> stocks = new LinkedList<Stock>();

		// Count the number of stocks in the center and sums the days of the
		// stocks to a result center days.
		for (Stock stock : values) {
			counter++;
			result = result.addStock(stock);
			stocks.add(new Stock(stock));
		}

		// Divide the result by the number of stocks in the center and round it.
		result = result.divide(counter).round();
		result.setName("");

		CenterWithCanopy newCenter = new CenterWithCanopy(result,
				key.getCanopyVector());
		centers.add(newCenter);

		// Check if there was a change between the last center result with the
		// current.
		if (result.compareTo(key.getStock()) != 0) {
			context.getCounter(Counter.CONVERGED).increment(1);
		} else {
			String newKeyString = newCenter.toString();
			for (Stock stock : stocks) {
				if (centersToStocks.get(newKeyString) == null) {
					centersToStocks.put(newKeyString, new ArrayList<String>());
				}

				centersToStocks.get(newKeyString).add(
						stock.getName().toString());
			}
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	protected void cleanup(Context context) throws IOException,
			InterruptedException {
		// Writes the new centers and to a file
		Configuration conf = context.getConfiguration();
		Path outPath = new Path(conf.get("centroid.path"));
		FileSystem fs = FileSystem.get(conf);
		fs.delete(outPath, true);

		final SequenceFile.Writer out = SequenceFile
				.createWriter(fs, context.getConfiguration(), outPath,
						Center.class, Center.class);

		for (CenterWithCanopy center : centers) {
			out.append(new Center(center.getCanopyVector().setName("canopy")),
					new Center(center.getStock().setName("")));
		}

		// Check if it's the last iteration - writes the output clustering
		// result!
		if (context.getCounter(Counter.CONVERGED).getValue() == 0) {
			for (String key : centersToStocks.keySet()) {
				Text uuid = new Text(UUID.randomUUID().toString());
				for (String symbol : centersToStocks.get(key)) {
					context.write(uuid, new Text(symbol));
				}
			}
		}

		out.close();
	}
}
