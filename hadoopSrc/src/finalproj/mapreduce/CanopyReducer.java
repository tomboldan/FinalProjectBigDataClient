package finalproj.mapreduce;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Reducer;

import de.jungblut.clustering.model.DistanceMeasurer;
import finalproj.Globals;
import finalproj.models.Center;

/**
 * Filter "noise" in the canopies found from each mapper.
 * 
 * <br>
 * Reducer &lt;1, center, 1, new center&gt;
 * 
 * @author Tom Boldan and Gal Schlezinger
 * 
 */
public class CanopyReducer extends
		Reducer<IntWritable, Center, IntWritable, Center> {

	private static ArrayList<Center> canopies = new ArrayList<Center>();

	@Override
	protected void reduce(IntWritable key, Iterable<Center> values,
			Context context) throws IOException, InterruptedException {
		boolean isCanopy = true;

		for (Center stock : values) {
			isCanopy = true;
			for (Center center : canopies) {
				double measureDistance = DistanceMeasurer.measureDistance(
						center, stock.getStock());
				if (measureDistance < Globals.T1) {
					isCanopy = false;
					break;
				}
			}

			if (isCanopy) {
				Center newCenter = new Center(stock);
				canopies.add(newCenter);
				context.write(new IntWritable(1), newCenter);
			}
		}
	}
}