package finalproj.mapreduce;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import de.jungblut.clustering.model.DistanceMeasurer;
import finalproj.Globals;
import finalproj.models.Center;
import finalproj.models.Stock;

/**
 * This class finds canopies centers by iterating over all the stocks and check
 * which stocks is not in the T1 radius of canopy center.
 * 
 * <br>
 * Mapper &lt;line index, line, 1, center&gt;
 * 
 * @author Tom Boldan and Gal Schlezinger
 * 
 */
public class CanopyMapper extends
		Mapper<LongWritable, Text, IntWritable, Center> {

	private static ArrayList<Center> canopies;

	@Override
	protected void setup(Context context) throws IOException,
			InterruptedException {
		if (canopies == null) {
			canopies = new ArrayList<Center>();
		}
	}

	@Override
	protected void map(LongWritable key, Text value, Context context)
			throws IOException, InterruptedException {
		Stock stock = Stock.parse(value.toString());
		boolean isCanopy = true;

		for (Center center : canopies) {
			double measureDistance = DistanceMeasurer.measureDistance(center,
					stock);
			if (measureDistance < Globals.T1) {
				isCanopy = false;
				break;
			}
		}

		if (isCanopy) {
			Stock canopyStock = new Stock(stock).setName("Canopy");
			Center newCenter = new Center(canopyStock);

			canopies.add(newCenter);
			context.write(new IntWritable(1), newCenter);
		}
	}
}
