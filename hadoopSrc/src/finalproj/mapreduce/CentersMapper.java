package finalproj.mapreduce;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import de.jungblut.clustering.model.DistanceMeasurer;
import finalproj.Globals;
import finalproj.models.Center;
import finalproj.models.Stock;


/**
 * Assign stocks to each canopy, and counter the number of stocks
 * 
 * <br>
 * Mapper &lt;line index, line, canopy, stock&gt;
 * 
 * @author Tom Boldan and Gal Schlezinger
 * 
 */
public class CentersMapper extends Mapper<LongWritable, Text, Center, Stock> {

	ArrayList<Center> canopies = new ArrayList<Center>();

	@Override
	protected void setup(Context context) throws IOException,
			InterruptedException {
		// Loads the canopies
		Configuration conf = context.getConfiguration();
		Path canopiesPath = new Path(conf.get("canopies.path"));
		FileSystem fs = FileSystem.get(conf);
		readCanopies(conf, canopiesPath, fs);
	}

	@SuppressWarnings("deprecation")
	/**
	 * Read canopies
	 * @param conf configuration
	 * @param centroids centers path file
	 * @param fs file system
	 * @throws IOException
	 */
	private void readCanopies(Configuration conf, Path centroids, FileSystem fs)
			throws IOException {
		SequenceFile.Reader reader = new SequenceFile.Reader(fs, centroids,
				conf);

		IntWritable key = new IntWritable();
		Center canopy = new Center();

		// Create map of canopy, centers
		while (reader.next(key, canopy)) {
			canopies.add(new Center(canopy));
		}

		reader.close();
	}

	@Override
	protected void map(LongWritable key, Text value, Context context)
			throws IOException, InterruptedException {
		Stock stock = Stock.parse(value.toString());

		// Finds the stocks that belong to the canopy center
		for (Center canopy : canopies) {
			double measureDistance = DistanceMeasurer.measureDistance(canopy,
					stock);
			if (measureDistance < Globals.T1) {
				Globals.STOCKS_NUMBER++;
				context.write(canopy, stock);
			}
		}
	}
}
