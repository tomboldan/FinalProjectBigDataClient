package finalproj.mapreduce;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import de.jungblut.clustering.model.DistanceMeasurer;
import finalproj.models.Center;
import finalproj.models.CenterWithCanopy;
import finalproj.models.Stock;

/**
 * This class creates new centers for KMeans algorithm.
 * 
 * <br>
 * Mapper &lt;line index, line, center with canopy, stock&gt;
 * 
 * @author Tom Boldan and Gal Schlezinger
 * 
 */
public class KMeansMapper extends
		Mapper<LongWritable, Text, CenterWithCanopy, Stock> {
	private HashMap<String, ArrayList<Center>> canopyToCenters;
	private List<Center> canopies;

	@Override
	protected void setup(Context context) throws IOException,
			InterruptedException {
		// Load the centers
		Configuration conf = context.getConfiguration();
		Path centroids = new Path(conf.get("centroid.path"));
		FileSystem fs = FileSystem.get(conf);

		canopies = new ArrayList<Center>();
		canopyToCenters = readCenters(conf, centroids, fs);
	}

	@SuppressWarnings("deprecation")
	/**
	 * Read canopies and centers.
	 * 
	 * @param conf configuration
	 * @param centroids centers path file
	 * @param fs file system
	 * @return Map of { canopy => [center, center, ...] }
	 * @throws IOException
	 */
	private HashMap<String, ArrayList<Center>> readCenters(Configuration conf,
			Path centroids, FileSystem fs) throws IOException {
		HashMap<String, ArrayList<Center>> canopyToCenters = new HashMap<String, ArrayList<Center>>();
		SequenceFile.Reader reader = new SequenceFile.Reader(fs, centroids,
				conf);

		Center canopy = new Center();
		Center center = new Center();

		// Create map of canopy, centers
		while (reader.next(canopy, center)) {
			String canopyName = canopy.toString();
			center.getStock().setName("");

			if (canopyToCenters.get(canopyName) == null) {
				canopyToCenters.put(canopyName, new ArrayList<Center>());
				canopies.add(new Center(canopy));
			}

			canopyToCenters.get(canopyName).add(new Center(center));
		}

		reader.close();
		return canopyToCenters;
	}

	@Override
	protected void map(LongWritable key, Text value, Context context)
			throws IOException, InterruptedException {
		Stock stock = Stock.parse(value.toString());

		Center nearest = null;
		double nearestDistance = Double.MAX_VALUE;

		Center nearestCanopy = getNearestCanopy(stock);
		String canopy = nearestCanopy.toString();

		// KMeans over the centers in the relevant canopy, therefore we run a
		// kmean clustering as a separate process.
		for (Center center : canopyToCenters.get(canopy)) {
			Center clusterCenter = new Center(center);

			double dist = DistanceMeasurer
					.measureDistance(clusterCenter, stock);

			if (nearestDistance > dist) {
				nearest = clusterCenter;
				nearestDistance = dist;
			}
		}

		CenterWithCanopy kmeansKey = new CenterWithCanopy(nearest,
				nearestCanopy.getStock());
		context.write(kmeansKey, stock);
	}

	/**
	 * Run over the canopies and find the nearest canopy to the current stock.
	 * 
	 * @param stock
	 * @return the nearest canopy
	 */
	private Center getNearestCanopy(Stock stock) {
		double nearestDistance = Double.MAX_VALUE;
		Center nearest = canopies.get(0);

		for (Center canopy : canopies) {
			Center clusterCenter = new Center(canopy);
			double dist = DistanceMeasurer
					.measureDistance(clusterCenter, stock);

			if (nearestDistance > dist) {
				nearest = clusterCenter;
				nearestDistance = dist;
			}
		}

		return nearest;
	}
}
