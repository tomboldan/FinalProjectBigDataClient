package finalproj.models;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

/**
 * A {@link Stock} day
 * 
 * @author Tom Boldan and Gal Schlezinger
 * 
 */
public class Day implements WritableComparable<Day> {
	private double[] features;
	private Text name;

	public Text getName() {
		return name;
	}

	public Day setName(Text name) {
		this.name = name;

		return this;
	}

	public Day setName(String name) {
		this.name = new Text(name);

		return this;
	}

	public Day() {
		super();
	}

	public Day(double... day) {
		super();
		setFeatures(day);
	}

	public int getSize() {
		return features.length;
	}

	public Day(List<Double> features, Text name) {
		double[] parseFeatures = new double[features.size()];

		for (int i = 0; i < features.size(); i++) {
			parseFeatures[i] = features.get(i);
		}

		setFeatures(parseFeatures);
		setName(name);
	}

	public Day(Day day) {
		this(day.getFeatures());
		setName(day.getName());
	}

	public double[] getFeatures() {
		return features;
	}

	public Day setFeatures(double[] features) {
		int size = features.length;
		this.features = new double[size];
		System.arraycopy(features, 0, this.features, 0, size);

		return this;
	}

	@Override
	public void write(DataOutput out) throws IOException {
		name.write(out);
		out.writeInt(features.length);

		for (int i = 0; i < features.length; i++) {
			out.writeDouble(features[i]);
		}
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		name = new Text();
		name.readFields(in);
		int size = in.readInt();

		features = new double[size];
		for (int i = 0; i < size; i++) {
			features[i] = in.readDouble();
		}
	}

	@Override
	public int compareTo(Day o) {
		for (int i = 0; i < features.length; i++) {
			double c = features[i] - o.features[i];
			if (c != 0.0d) {
				return (int) c;
			}
		}

		return 0;
	}

	@Override
	public String toString() {
		return "Day: " + name + " [features=" + Arrays.toString(features) + "]";
	}

	/**
	 * Create a default {@link Day}
	 * 
	 * @param size
	 *            features number
	 * @return {@link Day}
	 */
	public static Day createDefaultDay(int size) {
		double[] features = new double[size];

		for (int i = 0; i < size; i++) {
			features[i] = 0.0;
		}

		return new Day(features).setName("");
	}

	/**
	 * Summing 2 days
	 * 
	 * @param day
	 * @return {@link Day}
	 */
	public Day addStockDay(Day day) {
		Day newDay = Day.createDefaultDay(getSize());

		newDay.getFeatures()[0] = day.getFeatures()[0];

		for (int i = 1; i < getSize(); i++) {
			newDay.getFeatures()[i] = getFeatures()[i] + day.getFeatures()[i];
		}

		return newDay;
	}

	/**
	 * Divide all the features
	 * 
	 * @param num
	 *            the number to divide
	 * @return {@link Day}
	 */
	public Day divide(int num) {
		Day newDay = new Day(this);

		for (int i = 1; i < getSize(); i++) {
			newDay.getFeatures()[i] /= num;
		}

		return newDay;
	}

	/**
	 * Create a {@link Day} with the maximum value in every feature
	 * 
	 * @param day
	 * @return {@link Day}
	 */
	public Day maxUnion(Day day) {
		Day newDay = new Day(this);

		for (int i = 1; i < getSize(); i++) {
			newDay.getFeatures()[i] = Math.max(newDay.getFeatures()[i],
					day.getFeatures()[i]);
		}

		return newDay;
	}

	/**
	 * Create a {@link Day} with the minimum value in every feature
	 * 
	 * @param day
	 * @return {@link Day}
	 */
	public Day minUnion(Day day) {
		Day newDay = new Day(this);

		for (int i = 1; i < getSize(); i++) {
			newDay.getFeatures()[i] = Math.min(newDay.getFeatures()[i],
					day.getFeatures()[i]);
		}

		return newDay;
	}

	/**
	 * Round the day features
	 * 
	 * @return {@link Day}
	 */
	public Day round() {
		Day newDay = new Day(this);

		for (int i = 1; i < getSize(); i++) {
			newDay.getFeatures()[i] = Math.round(newDay.getFeatures()[i] * 100) / 100;
		}

		return newDay;
	}

	/**
	 * Parse a {@link Day} from a string
	 * 
	 * @param data
	 * @return {@link Day}
	 */
	public static Day parse(String data) {
		String[] strings = data.split(",");
		List<Double> features = new ArrayList<Double>();

		for (int i = 1; i < strings.length; i++) {
			// Check which features to include
			if (!strings[i].equals("@")) {
				features.add(Double.parseDouble(strings[i]));
			}
		}

		return new Day(features, new Text(strings[0]));
	}
}
