package finalproj.models;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

/**
 * Represents a stock that contains list of {@link Day} and symbol.
 * 
 * @author Tom Boldan and Gal Schlezinger
 * 
 */
public class Stock implements WritableComparable<Stock> {
	private List<Day> days;
	private Text name;

	public Stock() {
		days = null;
		name = null;
	}

	public Stock(Stock stock) {
		days = new ArrayList<Day>();

		for (Day stockVec : stock.getDays()) {
			days.add(new Day(stockVec));
		}

		name = stock.getName();
	}

	public List<Day> getDays() {
		return days;
	}

	public Stock setDays(List<Day> days) {
		this.days = days;

		return this;
	}

	public Text getName() {
		return name;
	}

	public Stock setName(Text name) {
		this.name = name;

		return this;
	}

	public Stock setName(String name) {
		this.name = new Text(name);

		return this;
	}

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeInt(days.size());
		name.write(out);

		for (Day day : days) {
			day.write(out);
		}
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		int size = in.readInt();
		name = new Text();
		name.readFields(in);

		days = new ArrayList<Day>();

		for (int i = 0; i < size; i++) {
			Day stock = new Day();
			stock.readFields(in);
			days.add(stock);
		}
	}

	@Override
	public int compareTo(Stock o) {
		for (int i = 0; i < days.size(); i++) {

			int c = days.get(i).compareTo(o.getDays().get(i));
			if (c != 0) {
				return (int) c;
			}
		}

		return 0;
	}

	/**
	 * Create a default stock
	 * 
	 * @see Day#createDefaultDay(int)
	 * @param numberOfDays
	 * @param numberOfFeatures
	 * @return {@link Stock}
	 */
	public static Stock createDefaultStock(int numberOfDays,
			int numberOfFeatures) {
		Stock data = new Stock();
		data.setName("");
		List<Day> days = new ArrayList<Day>();

		for (int i = 0; i < numberOfDays; i++) {
			days.add(Day.createDefaultDay(numberOfFeatures));
		}

		data.setDays(days);

		return data;
	}

	@Override
	public String toString() {
		StringBuilder strb = new StringBuilder();

		for (Day day : days) {
			strb.append(day.toString()).append(", ");
		}

		return "[ " + strb.toString() + " ]";
	}

	/**
	 * Summing a {@link Stock} by summing their days
	 * 
	 * @see Day#addStockDay(Day)
	 * @param stock
	 * @return {@link Stock}
	 */
	public Stock addStock(Stock stock) {
		Stock newStock = new Stock(this);

		for (int i = 0; i < this.getDays().size(); i++) {
			Day thisDay = this.getDays().get(i);
			Day addDay = stock.getDays().get(i);
			newStock.getDays().set(i, thisDay.addStockDay(addDay));
		}

		return newStock;
	}

	/**
	 * Dividing a stock days
	 * 
	 * @see Day#divide(int)
	 * @param num
	 *            the number to divide
	 * @return {@link Stock}
	 */
	public Stock divide(int num) {
		Stock newStock = new Stock(this);
		newStock.setName("");

		for (int i = 0; i < this.getDays().size(); i++) {
			Day thisDay = this.getDays().get(i);
			newStock.getDays().set(i, thisDay.divide(num));
		}

		return newStock;
	}

	/**
	 * Create a {@link Stock} which his days are the maximal days
	 * 
	 * @see Day#maxUnion(Day)
	 * @param stock
	 * @return {@link Stock}
	 */
	public Stock max(Stock stock) {
		Stock newStock = new Stock(this);

		for (int i = 0; i < this.getDays().size(); i++) {
			Day thisDay = this.getDays().get(i);
			Day otherDay = stock.getDays().get(i);
			newStock.getDays().set(i, thisDay.maxUnion(otherDay));
		}

		return newStock;
	}

	/**
	 * Create a {@link Stock} which his days are the minimum days
	 * 
	 * @see Day#minUnion(Day)
	 * @param stock
	 * @return {@link Stock}
	 */
	public Stock min(Stock stock) {
		Stock newStock = new Stock(this);

		for (int i = 0; i < this.getDays().size(); i++) {
			Day thisDay = this.getDays().get(i);
			Day otherDay = stock.getDays().get(i);
			newStock.getDays().set(i, thisDay.minUnion(otherDay));
		}

		return newStock;
	}

	/**
	 * Round the stock days
	 * 
	 * @see Day#round()
	 * @return {@link Stock}
	 */
	public Stock round() {
		Stock newStock = new Stock(this);

		for (int i = 0; i < newStock.getDays().size(); i++) {
			newStock.getDays().set(i, newStock.getDays().get(i).round());
		}

		return newStock;
	}

	/**
	 * Parse a {@link Stock} from string
	 * 
	 * @see Day#parse(String)
	 * @param data
	 * @return {@link Stock}
	 */
	public static Stock parse(String data) {
		String[] dataStr = data.split(";");

		List<Day> days = new ArrayList<Day>();

		for (String str : dataStr) {
			days.add(Day.parse(str));
		}

		return new Stock().setDays(days).setName(days.get(0).getName());
	}
}
