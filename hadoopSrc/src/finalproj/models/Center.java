package finalproj.models;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;

/**
 * Represent a {@link Stock} center
 * 
 * @author Tom Boldan and Gal Schlezinger
 * 
 */
public class Center implements WritableComparable<Center> {
	private Stock stock;

	public Center() {
		stock = new Stock();
	}

	public Center(Center center) {
		stock = new Stock(center.getStock());
	}

	public Center(Stock stock) {
		this.stock = new Stock(stock);
	}

	public Stock getStock() {
		return stock;
	}

	public Center setStock(Stock stock) {
		this.stock = stock;

		return this;
	}

	@Override
	public void write(DataOutput out) throws IOException {
		stock.write(out);
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		stock = new Stock();
		stock.readFields(in);
	}

	@Override
	public int compareTo(Center o) {
		return stock.compareTo(o.getStock());
	}

	/**
	 * Create a default center
	 * 
	 * @param numberOfDays
	 * @param numberOfFeatures
	 * @return {@link Center}
	 */
	public static Center createDefaultCenter(int numberOfDays,
			int numberOfFeatures) {
		Center center = new Center();
		center.setStock(Stock
				.createDefaultStock(numberOfDays, numberOfFeatures));

		return center;
	}

	@Override
	public String toString() {
		return "Center: [" + this.stock.toString() + "]";
	}
}
