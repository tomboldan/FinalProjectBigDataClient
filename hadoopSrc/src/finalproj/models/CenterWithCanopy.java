package finalproj.models;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Text;

/**
 * Represent a {@link Center} with a canopy
 * 
 * @author Tom Boldan and Gal Schlezinger
 * 
 */
public class CenterWithCanopy extends Center {
	private Stock canopy;

	public Text getCanopy() {
		return new Text(canopy.toString());
	}

	public Stock getCanopyVector() {
		return canopy;
	}

	public void setCanopy(Stock canopy) {
		this.canopy = new Stock(canopy);
	}

	public CenterWithCanopy() {
		super();
		this.canopy = new Stock();
	}

	public CenterWithCanopy(Stock center, Stock canopy) {
		super(center);
		this.canopy = new Stock(canopy);
	}

	public CenterWithCanopy(Center center, Stock canopy) {
		super(center);
		this.canopy = new Stock(canopy);
	}

	@Override
	public void write(DataOutput out) throws IOException {
		super.write(out);
		canopy.write(out);
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		super.readFields(in);
		canopy.readFields(in);
	}

	public int compareTo(CenterWithCanopy o) {
		int canopyCompare = o.getCanopy().equals(o.getCanopy()) ? 0 : -1;
		int centerCompare = o.getStock().compareTo(o.getStock()) == 0 ? 0 : -1;

		return canopyCompare != 0 || centerCompare != 0 ? -1 : 0;
	}

	@Override
	public String toString() {
		return "Canopy: " + this.canopy.toString() + " " + super.toString();
	}
}
