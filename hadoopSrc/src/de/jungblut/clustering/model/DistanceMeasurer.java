package de.jungblut.clustering.model;

import finalproj.models.Center;
import finalproj.models.Day;
import finalproj.models.Stock;

/**
 * Provides a service to measure distances.
 * 
 * @author Tom Boldan and Gal Schlezinger
 */
public class DistanceMeasurer {
	/**
	 * Measures the distance between a center to another stock
	 * 
	 * @param center
	 *            to measure the distance from.
	 * @param stock
	 *            to measure the distance to.
	 * @return double containing the distance between the two.
	 */
	public static final double measureDistance(Center center, Stock stock) {
		double sum = 0;

		for (int vecIndex = 0; vecIndex < stock.getDays().size(); vecIndex++) {
			Day dataDay = stock.getDays().get(vecIndex);
			Day centerDay = center.getStock().getDays().get(vecIndex);

			double currSum = 0;
			for (int dataIndex = 0; dataIndex < centerDay.getSize(); dataIndex++) {
				currSum += Math.abs(centerDay.getFeatures()[dataIndex]
						- dataDay.getFeatures()[dataIndex]);
			}

			sum += currSum / centerDay.getSize();
		}

		return sum;
	}
}
