package finalproj.stocks;

import java.io.IOException;

import org.junit.Test;

import finalproj.models.AnalysisFeatures;

public class StockRunnerTest {

	@Test
	public void runGood() throws IOException {
		// Assume
		AnalysisFeatures features = new AnalysisFeatures().setOpen(true).setHigh(true).setLow(true).setClose(true);

		// Act
		String result = StockRunner.run(100, 30, features, 7);

		// System.out.println(result);
	}
}
