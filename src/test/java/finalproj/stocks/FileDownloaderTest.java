package finalproj.stocks;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import finalproj.models.StockDay;
import finalproj.models.StockSymbol;

public class FileDownloaderTest {

	@Test
	public void canGetStringsFromUrls() throws IOException {
		// Act
		String result = FileDownloader.getStringFromURL(
				"https://gist.githubusercontent.com/tomBold/d4f86fa1321b0f1439d7/raw/f0f19bf2773dd98f337f1e51fb5b2c312202e5ca/hello.txt");
		String secondHello = FileDownloader.getStringFromURL(
				"https://gist.githubusercontent.com/tomBold/f8e037c9d5d9ef0bc832/raw/e63b193320b069e017f57cb15e4c0db1770c3ca8/secondhello.txt");
		String helloWithEnter = FileDownloader.getStringFromURL(
				"https://gist.githubusercontent.com/tomBold/3889fcc98c4bb92abf5d/raw/34ace5e5bfae77e65409ffe15ce392861533a874/helloWithEnter");

		// Assert
		assertEquals(result, "hello");
		assertEquals(secondHello, "second hello");
		assertEquals(helloWithEnter, "hello\nworld");
	}

	@Test
	public void canGetStockData() throws IOException {
		// Act
		List<StockDay> stockData = FileDownloader.getStockDays(1, "AAAP");

		// Assert
		Assert.assertNotNull(stockData);
		Assert.assertFalse(stockData.size() == 0);
	}

	@Test
	public void canGetStockSymbols() throws IOException {
		// Act
		List<StockSymbol> stockSymbols = FileDownloader.getStockSymbols(1);

		// Assert
		Assert.assertNotNull(stockSymbols);
		Assert.assertTrue(stockSymbols.size() == 1);
	}

	@Test
	public void canGetQuerystringForDays() {
		// Assume
		LocalDate date = LocalDate.parse("2016-01-01");

		// Act
		String oneDayBefore = FileDownloader.getQuerystringForDays(1, date);

		// Assert
		assertEquals(oneDayBefore, "a=11&b=30&c=2015");
	}
}
