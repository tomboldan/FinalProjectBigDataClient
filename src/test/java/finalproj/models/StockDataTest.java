package finalproj.models;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Test;

public class StockDataTest {

	@Test
	public void canCreateStockDataFromString() {
		// Assume
		String dataStr = "2015-12-31,769.50,769.50,758.340027,758.880005,1489600,758.880005";
		long date = LocalDate.parse("2015-12-31").toEpochDay();

		// Act
		StockDay stockData = StockDay.parseStockDayFromSring(dataStr, "x");

		// Assert
		assertEquals(date, stockData.getDate());
		assertEquals(String.valueOf(stockData.getOpen()), "769.5");
		assertEquals(String.valueOf(stockData.getHigh()), "769.5");
		assertEquals(String.valueOf(stockData.getLow()), "758.340027");
		assertEquals(String.valueOf(stockData.getClose()), "758.880005");
		assertEquals(String.valueOf(stockData.getVolume()), "1489600");
		assertEquals(String.valueOf(stockData.getAdjClose()), "758.880005");
	}
}
