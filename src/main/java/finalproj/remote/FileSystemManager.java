package finalproj.remote;

import java.util.UUID;

import finalproj.models.RemoteDirectory;

/**
 * This class manages the remote file system on the cloudera VM
 * 
 * @author Tom Boldan and Gal Schlezinger
 */
public class FileSystemManager extends ProcessManager {
	/**
	 * Create a randomly generated directory, with sub directories for the
	 * source and the data.
	 * 
	 * @return {@link String} contains the newly created directory path
	 */
	public RemoteDirectory createDirectory() {
		RemoteDirectory remoteDir = new RemoteDirectory("/tmp/" + UUID.randomUUID());

		plink("mkdir " + remoteDir.base() + " " + remoteDir.data() + " " + remoteDir.src());

		return remoteDir;
	}

	/**
	 * Transfers all the files from
	 * 
	 * <pre>
	 * localDirectoryPath
	 * </pre>
	 * 
	 * to
	 * 
	 * <pre>
	 * remoteDirectoryPath
	 * </pre>
	 * 
	 * @param localDirectoryPath
	 *            local directory to copy the files from
	 * @param remoteDirectoryPath
	 *            remote directory to copy the files to
	 */
	public void transferDirectoryContents(String localDirectoryPath, String remoteDirectoryPath) {
		pscp(localDirectoryPath + "/*", remoteDirectoryPath);
	}

	public void copyFileFromVM(String fromPath, String toPath) {
		pscpFromServer(fromPath, toPath);
	}
}
