package finalproj.remote;

import java.util.UUID;

/**
 * This class manages the hadoop interaction on the VM
 * 
 * @author Tom Boldan and Gal Schlezinger
 *
 */
public class HadoopProcessManager extends ProcessManager {
	/**
	 * Compiles the java files on the specified path
	 * 
	 * @param dirPath
	 *            with *.java files
	 */
	public void compileJava(String dirPath) {
		plink("cd " + dirPath + " && (find -name \"*.java\" | xargs javac -classpath `hadoop classpath`)");
	}

	/**
	 * Creates a jar for the *.class files in the specified directory
	 * 
	 * @param dirPath
	 *            with *.class files
	 * @return The path for the newly created JAR file
	 */
	public String createJar(String dirPath) {
		String jarPath = "/tmp/" + UUID.randomUUID() + ".jar";
		plink("cd " + dirPath + " && (find -name \"*.class\" | xargs jar cvf " + jarPath + ")");

		return jarPath;
	}

	/**
	 * Executes the jar file provided on Hadoop, using the main on EX3Driver
	 * 
	 * @param jarPath
	 *            to execute
	 * @param basePath
	 *            HDFS input path
	 * @param outputPath
	 *            HDFS output path
	 */
	public void executeJar(String jarPath, String basePath, String driver, String... params) {
		plink("hadoop jar " + jarPath + " " + driver + " " + basePath + " " + " " + String.join(" ", params));
	}

	/**
	 * Copies the files from the remote VM to HDFS
	 * 
	 * @param remotePath
	 *            to copy the files from
	 * @param hdfsPath
	 *            to copy the files to
	 */
	public void copyToHDFS(String remotePath, String hdfsPath) {
		plink("hadoop fs -put " + remotePath + " " + hdfsPath);
	}

	/**
	 * Creates a directory on HDFS with the path specified
	 * 
	 * @param path
	 *            to create on HDFS
	 */
	public void createHDFSDirectories(String path) {
		plink("hadoop fs -mkdir " + path);
	}

	/**
	 * Prints the Hadoop output for the specified directory
	 * 
	 * @param outputPath
	 *            to print the
	 * 
	 *            <pre>
	 *            part - r - 00000
	 *            </pre>
	 * 
	 *            file in it
	 */
	public void readOutput(String outputPath) {
		plink("hadoop fs -cat " + outputPath + "/part-r-00000");
	}

	/**
	 * Copy a file form hdfs to the remote machine
	 * 
	 * @param hadoopPath
	 * @param remotePath
	 */
	public void copyFromHDFSToRemote(String hadoopPath, String remotePath) {
		plink("hadoop fs -cat " + hadoopPath + " > " + remotePath);
	}
}
