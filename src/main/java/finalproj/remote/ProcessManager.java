package finalproj.remote;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Logger;

/**
 * This is a base class for managing processes in our application
 * 
 * @author Tom Boldan and Gal Schlezinger
 *
 */
public abstract class ProcessManager {
	public static String user;
	public static String password;
	public static String hostname;

	private static Logger logger = Logger.getLogger(ProcessManager.class.getName());

	public String getUser() {
		return user;
	}

	public static void setUser(String user) {
		ProcessManager.user = user;
	}

	public String getPassword() {
		return password;
	}

	public static void setPassword(String password) {
		ProcessManager.password = password;
	}

	public String getHostname() {
		return hostname;
	}

	public static void setHostname(String hostname) {
		ProcessManager.hostname = hostname;
	}

	/**
	 * Runs a command and pipes the command's outputs to stdout
	 * 
	 * @param command
	 *            to run
	 */
	public void runCommand(String command) {
		Runtime runtime = Runtime.getRuntime();
		try {
			Process proc = runtime.exec(command);

			Thread tInput = pipe(proc.getInputStream());
			Thread tError = pipe(proc.getErrorStream());

			tInput.join();
			tError.join();
		} catch (IOException e) {
			logger.severe(e.getMessage());
		} catch (InterruptedException e) {
			logger.severe(e.getMessage());
		}
	}

	/**
	 * Pipe the output to the application's standard output, asynchronously
	 * 
	 * @param input
	 *            to stream
	 * @return {@link Thread} that listens to the sub process' output
	 */
	private Thread pipe(InputStream input) {
		Thread t = new Thread() {
			@Override
			public void run() {
				try {
					byte[] buffer = new byte[1024];
					int len = input.read(buffer);

					while (len != -1) {
						System.out.write(buffer, 0, len);
						len = input.read(buffer);
					}
				} catch (IOException e) {
					logger.severe(e.getMessage());
				}
			}
		};
		t.start();
		return t;
	}

	/**
	 * Run plink - run a command remotly
	 * 
	 * @param command
	 *            to run remotely
	 */
	protected void plink(String command) {
		runCommand(String.format("putty\\plink.exe -pw %s %s@%s \"%s\"", getPassword(), getUser(), getHostname(),
				command));
	}

	/**
	 * Run pscp - to transfer files to the remote server
	 * 
	 * @param path
	 *            - local path
	 * @param toPath
	 *            - remote path
	 */
	protected void pscp(String path, String toPath) {
		runCommand(String.format("putty\\pscp.exe -r -pw %s %s %s@%s:%s", getPassword(), path, getUser(), getHostname(),
				toPath));
	}

	/**
	 * Run pscp - to transfer file from remote server
	 * 
	 * @param fromPath
	 *            - remote path
	 * @param toPath
	 *            - local path
	 */
	protected void pscpFromServer(String fromPath, String toPath) {
		runCommand(String.format("putty\\pscp.exe -r -pw %s %s@%s:%s %s", getPassword(), getUser(), getHostname(),
				fromPath, toPath));
	}
}
