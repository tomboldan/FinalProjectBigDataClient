package finalproj.controllers;

import finalproj.models.AnalysisFeatures;
import finalproj.stocks.StockRunner;
import spark.Request;
import spark.Response;
import spark.Route;

/**
 * Route to call the clustering process and get the results
 *
 * @author Tom Boldan and Gal Schlezinger
 */
public class HandleDataRoute implements Route {
	/**
	 * Handle the requests data
	 * 
	 * @param req
	 *            {@link Request}
	 * @param res
	 *            {@link Response}
	 * @return {@link Object} json as string
	 * @throws Exception
	 */
	@Override
	public Object handle(Request req, Response res) throws Exception {
		int stocks = getIntParam(req, "stocks");
		int days = getIntParam(req, "days");
		int clusters = getIntParam(req, "clusters");

		AnalysisFeatures features = new AnalysisFeatures().setOpen(getBooleanParam(req, "open"))
				.setClose(getBooleanParam(req, "close")).setLow(getBooleanParam(req, "low"))
				.setHigh(getBooleanParam(req, "high"));

		return StockRunner.run(stocks, days, features, clusters);
	}

	/**
	 * Get a query parameter as a positive {@link Integer}
	 *
	 * @see Request#queryParams(String)
	 * @param req
	 *            {@link Request}
	 * @param paramField
	 *            the parameter to query
	 * @return {@link Integer} contains the positive parameter
	 * @throws IllegalArgumentException
	 */
	private int getIntParam(Request req, String paramField) throws Exception {
		int numParam = Integer.parseInt(req.queryParams(paramField));
		if (numParam <= 0) {
			throw new IllegalArgumentException(paramField + " must be a positive number!");
		}

		return numParam;
	}

	/**
	 * Get a query parameter as a {@link Boolean}
	 *
	 * @param req
	 *            {@link Request}
	 * @param paramField
	 *            the parameter to query
	 * @see Request#queryParams(String)
	 * @return whether the value is "true"
	 */
	private boolean getBooleanParam(Request req, String paramField) {
		return req.queryParams(paramField).equals("true");
	}
}
