package finalproj.stocks;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import com.google.gson.Gson;

import finalproj.models.AnalysisFeatures;
import finalproj.models.ClusterResult;
import finalproj.models.RemoteDirectory;
import finalproj.models.StockDay;
import finalproj.models.StockResult;
import finalproj.models.StockSymbol;
import finalproj.remote.FileSystemManager;
import finalproj.remote.HadoopProcessManager;
import finalproj.remote.ProcessManager;
import finalproj.utils.EnvUtils;

/**
 * This class is in charge of the server-hadoop communication
 * 
 * @author Tom Boldan and Gal Schlezinger
 */
public class StockRunner {
	private static FileSystemManager fsMgr;
	private static HadoopProcessManager hadoopMgr;
	private static Logger logger = Logger.getLogger(StockRunner.class.getName());

	/**
	 * Connection information
	 */
	static {
		ProcessManager.setUser(EnvUtils.user());
		ProcessManager.setPassword(EnvUtils.password());
		ProcessManager.setHostname(EnvUtils.host());

		fsMgr = new FileSystemManager();
		hadoopMgr = new HadoopProcessManager();
	};

	/**
	 * Runs the final project.
	 * 
	 * @param stocks
	 *            number to grab from the remote nasdaq file
	 * @param days
	 *            number to fetch stock days backwards
	 * @param features
	 *            to classify in the Map/Reduce
	 * @param clusters
	 *            number to split the data into
	 * @return {@link String} contains the output of the Map/Reduce
	 * @throws IOException
	 */
	public static String run(int stocks, int days, AnalysisFeatures features, int clusters) throws IOException {
		logger.info("Downloading the stocks days");
		List<StockSymbol> stockSymbols = FileDownloader.getStockSymbols(stocks);

		// Prepare data for clustering process
		downloadStockDays(days, stockSymbols);
		List<StockSymbol> normaStocks = normalizeStocks(stockSymbols);
		Map<String, List<StockDay>> symbolNameToStockDays = groupStocksBySymbolName(normaStocks);

		String resultPath = executeClusteringRemotely(features, clusters, normaStocks);

		logger.info("Reading the local files");
		return readOutputFile(resultPath, symbolNameToStockDays);
	}

	/**
	 * Execute the clustering on the remote hadoop
	 * 
	 * @param features
	 * @param clusters
	 * @param normaStocks
	 * @return the local result path
	 * @throws IOException
	 */
	private static String executeClusteringRemotely(AnalysisFeatures features, int clusters,
			List<StockSymbol> normaStocks) throws IOException {
		Path tempDirectory = Files.createTempDirectory(Messages.getString("StockRunner.tempDirPrefix"));
		List<List<StockSymbol>> sliceStocks = sliceStocks(normaStocks);
		prepareFilesForHadoop(features, sliceStocks, tempDirectory);

		RemoteDirectory remoteDir = fsMgr.createDirectory();
		logger.info("Data will be saved to " + remoteDir.base());

		String jarPath = prepareHadoop(tempDirectory, remoteDir);

		logger.info("Starting to execute");
		hadoopMgr.executeJar(jarPath, remoteDir.base(), Messages.getString("StockRunner.jarMainClass"),
				String.valueOf(clusters));

		hadoopMgr.copyFromHDFSToRemote(remoteDir.kmeansResult(), remoteDir.output());
		String resultPath = tempDirectory.toString() + Messages.getString("StockRunner.localTempResult");
		fsMgr.copyFileFromVM(remoteDir.output(), resultPath);

		return resultPath;
	}

	/**
	 * Prepare the data and the jar for hadoop
	 * 
	 * @param tempDirectory
	 *            the local temp data directory
	 * @param remoteDir
	 *            the remote temp directory
	 * @return the jar path
	 */
	private static String prepareHadoop(Path tempDirectory, RemoteDirectory remoteDir) {
		fsMgr.transferDirectoryContents(tempDirectory.toString(), remoteDir.data());
		fsMgr.transferDirectoryContents(Messages.getString("StockRunner.hadoopSrcPath"), remoteDir.src());

		logger.info("Compiling java");
		hadoopMgr.compileJava(remoteDir.src());
		String jarPath = hadoopMgr.createJar(remoteDir.src());

		logger.info("Copy to hadoop");
		hadoopMgr.copyToHDFS(remoteDir.base(), remoteDir.base());

		return jarPath;
	}

	/**
	 * Groups the stocks by their symbol name
	 * 
	 * @param stocks
	 * @return a map which the key is the symbol name and the value is a list of
	 *         {@link StockDay}
	 */
	private static Map<String, List<StockDay>> groupStocksBySymbolName(List<StockSymbol> stocks) {
		return stocks.parallelStream().filter(x -> x.getStockDays() != null).flatMap(x -> x.getStockDays().stream())
				.collect(Collectors.groupingBy(StockDay::getSymbol, Collectors.toList()));
	}

	/**
	 * Normalize the stocks
	 * 
	 * @param stockSymbols
	 * @return a list of normalize stocks {@link StockSymbol}
	 */
	private static List<StockSymbol> normalizeStocks(List<StockSymbol> stockSymbols) {
		return stockSymbols.parallelStream().filter(x -> x.getStockDays() != null).map(StockSymbol::normalize)
				.collect(Collectors.toList());
	}

	/**
	 * Downloads the {@link StockDay} for every {@link StockSymbol}
	 * 
	 * @param days
	 * @param stockSymbols
	 */
	private static void downloadStockDays(int days, List<StockSymbol> stockSymbols) {
		stockSymbols.parallelStream().unordered().forEach(symbol -> {
			try {
				symbol.fetchStockDays(days);
			} catch (Exception e) {
				logger.severe(e.getMessage());
			}
		});
	}

	/**
	 * Read output clustering file
	 * 
	 * @param outputClusteringFile
	 * @param groupStocks
	 * @return json string
	 * @throws IOException
	 */
	public static String readOutputFile(String outputClusteringFile, Map<String, List<StockDay>> groupStocks)
			throws IOException {
		List<String> lines = Files.readAllLines(Paths.get(outputClusteringFile));

		Collection<List<StockResult>> clusterToSymbols = lines.stream().map(ClusterResult::parseString)
				.collect(Collectors.groupingBy(ClusterResult::getCluster, Collectors.mapping(
						x -> StockResult.parse(groupStocks.get(((ClusterResult) x).getSymbol())), Collectors.toList())))
				.values();

		return new Gson().toJson(clusterToSymbols);
	}

	/**
	 * Creates files in the {@link Path} provided. each {@link StockSymbol} gets
	 * his own file, containing the result of
	 * {@link StockDay#toString(AnalysisFeatures, String)} for every stock data.
	 * 
	 * @param features
	 *            to remain on the map/reduce
	 * @param data
	 *            the stocks data
	 * @param tempDirectory
	 *            to write the files to
	 * @throws IOException
	 */
	private static void prepareFilesForHadoop(AnalysisFeatures features, List<List<StockSymbol>> data,
			Path tempDirectory) throws IOException {
		for (List<StockSymbol> file : data) {
			String fileContents = file.stream().map(x -> x.toString(features)).collect(Collectors.joining("\n")) + "\n";

			Path tempFile = Files.createTempFile(tempDirectory, Messages.getString("StockRunner.tempDirPrefix"), "");
			Files.write(tempFile, fileContents.getBytes());
		}
	}

	/**
	 * Slice the stocks to group of 10 members
	 * 
	 * @param stocks
	 * @return the list of the groups
	 */
	private static List<List<StockSymbol>> sliceStocks(List<StockSymbol> stocks) {
		List<List<StockSymbol>> result = new ArrayList<>();
		result.add(new ArrayList<>());

		for (StockSymbol stock : stocks) {
			if (result.get(result.size() - 1).size() >= 10) {
				result.add(new ArrayList<>());
			}

			result.get(result.size() - 1).add(stock);
		}

		return result;
	}
}
