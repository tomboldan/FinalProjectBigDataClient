package finalproj.stocks;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import finalproj.models.StockDay;
import finalproj.models.StockSymbol;

/**
 * Provides file downloading capabilities
 * 
 * @author Tom Boldan and Gal Schlezinger
 */
public class FileDownloader {
	private static Logger logger = Logger.getLogger(FileDownloader.class.getName());

	/**
	 * This method gets the contents of a URL text file
	 * 
	 * @param urlPath
	 *            The URL for the text file
	 * @return {@link String} containing the text file contents
	 * @throws IOException
	 */
	public static String getStringFromURL(String urlPath) throws IOException {
		URL url = new URL(urlPath);
		logger.info("Beginning to download " + urlPath);
		Scanner responseScanner;
		try {
			responseScanner = new Scanner(url.openStream());
		} catch (FileNotFoundException e) {
			logger.severe(e.getMessage());

			return null;
		}

		StringBuilder stringBuilder = new StringBuilder();
		boolean firstLine = true;

		// Read all the lines
		while (responseScanner.hasNextLine()) {
			if (!firstLine) {
				stringBuilder.append("\n");
			}
			stringBuilder.append(responseScanner.nextLine());
			firstLine = false;
		}

		responseScanner.close();

		logger.info("Finished downloading " + urlPath);

		return stringBuilder.toString();
	}

	/**
	 * This method create the string query with day filter
	 * 
	 * @param dayNum
	 *            - the numbers of days
	 * @param currentDate
	 *            - the current date
	 * @return {@link String} the string query with day filter
	 */
	public static String getQuerystringForDays(int dayNum, LocalDate currentDate) {
		LocalDate dateMinusDays = currentDate.minusDays(dayNum + 1);
		int month = dateMinusDays.getMonthValue() - 1;
		int day = dateMinusDays.getDayOfMonth();
		int year = dateMinusDays.getYear();

		return String.format("a=%d&b=%d&c=%d", month, day, year);
	}

	/**
	 * This method gets the stock days
	 * 
	 * @param dayNum
	 *            - the number of days
	 * @param symbol
	 *            - the stock symbol name
	 * @return the stock days
	 * @throws IOException
	 */
	public static List<StockDay> getStockDays(int dayNum, String symbol) throws IOException {
		String stockDaysUrl = Messages.getString("FileDownloader.getStockDaysUrl");
		String querystringForDays = getQuerystringForDays(dayNum, LocalDate.now());
		String url = String.format(stockDaysUrl, symbol, querystringForDays);

		String data = getStringFromURL(url);

		if (data == null) {
			return null;
		}

		String[] dataRows = data.split("\n");

		List<String> rows = Arrays.asList(Arrays.copyOfRange(dataRows, 1, dataRows.length));
		List<StockDay> stockDays = rows.stream().map(x -> StockDay.parseStockDayFromSring(x, symbol))
				.sorted(Comparator.comparing(StockDay::getDate)).collect(Collectors.toList());

		return stockDays;
	}

	/**
	 * This method gets the stock symbols
	 * 
	 * @param numberOfStocks
	 *            - the number of stock symbols
	 * @return the stock symbols
	 * @throws IOException
	 */
	public static List<StockSymbol> getStockSymbols(int numberOfStocks) throws IOException {
		String stockSymbolsUrl = Messages.getString("FileDownloader.getStockSymbolsUrl");
		String data = getStringFromURL(stockSymbolsUrl);
		String[] dataRows = data.split("\n");

		List<String> rows = Arrays.asList(Arrays.copyOfRange(dataRows, 1, dataRows.length));
		List<StockSymbol> stockSymbols = rows.stream().limit(numberOfStocks)
				.map(StockSymbol::parstStockSymbolFromString).collect(Collectors.toList());

		return stockSymbols;
	}
}
