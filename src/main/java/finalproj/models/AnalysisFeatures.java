package finalproj.models;

/**
 * Combining all the analysis features our program needs to know from the user
 * request
 * 
 * @author Tom Boldan and Gal Schlezinger
 */
public class AnalysisFeatures {
	private boolean open;
	private boolean close;
	private boolean high;
	private boolean low;

	public boolean isOpen() {
		return open;
	}

	public AnalysisFeatures setOpen(boolean open) {
		this.open = open;

		return this;
	}

	public boolean isClose() {
		return close;
	}

	public AnalysisFeatures setClose(boolean close) {
		this.close = close;

		return this;
	}

	public boolean isHigh() {
		return high;
	}

	public AnalysisFeatures setHigh(boolean high) {
		this.high = high;

		return this;
	}

	public boolean isLow() {
		return low;
	}

	public AnalysisFeatures setLow(boolean low) {
		this.low = low;

		return this;
	}
}
