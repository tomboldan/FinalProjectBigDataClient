package finalproj.models;

import java.util.List;

/**
 * Contains the minimum and maximum {@link StockDay} values
 * 
 * @author Tom Boldan and Gal Schlezinger
 *
 */
public class StockDayRange {
	StockDay min;
	StockDay max;

	public StockDay getMin() {
		return min;
	}

	public StockDayRange setMin(StockDay min) {
		this.min = min;

		return this;
	}

	public StockDay getMax() {
		return max;
	}

	public StockDayRange setMax(StockDay max) {
		this.max = max;

		return this;
	}

	/**
	 * Calculates the minimum and maximum {@link StockDay} values
	 * 
	 * @param stocks
	 * @return {@link StockDayRange}
	 */
	public static StockDayRange create(List<StockDay> stocks) {
		StockDay min = new StockDay(stocks.get(0));
		StockDay max = new StockDay(stocks.get(0));

		for (StockDay stock : stocks) {
			min.setOpen(Math.min(stock.getOpen(), min.getOpen()));
			min.setClose(Math.min(stock.getClose(), min.getClose()));
			min.setHigh(Math.min(stock.getHigh(), min.getHigh()));
			min.setLow(Math.min(stock.getLow(), min.getLow()));

			max.setOpen(Math.max(stock.getOpen(), max.getOpen()));
			max.setClose(Math.max(stock.getClose(), max.getClose()));
			max.setHigh(Math.max(stock.getHigh(), max.getHigh()));
			max.setLow(Math.max(stock.getLow(), max.getLow()));
		}

		return new StockDayRange().setMax(max).setMin(min);
	}
}
