package finalproj.models;

/**
 * The cluster result
 * 
 * @author Tom Boldan and Gal Schlezinger
 */
public class ClusterResult {
	String symbol;
	String cluster;

	public String getSymbol() {
		return symbol;
	}

	public ClusterResult setSymbol(String symbol) {
		this.symbol = symbol;

		return this;
	}

	public String getCluster() {
		return cluster;
	}

	public ClusterResult setCluster(String cluster) {
		this.cluster = cluster;

		return this;
	}

	/**
	 * Parse a {@link ClusterResult} from string
	 * 
	 * @param str
	 * @return
	 */
	public static ClusterResult parseString(String str) {
		String[] split = str.split("\t");

		return new ClusterResult().setCluster(split[0]).setSymbol(split[1]);
	}
}
