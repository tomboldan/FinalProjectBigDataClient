package finalproj.models;

import java.time.LocalDate;

/**
 * Represents stock day. Class for representing a line in
 * "http://ichart.yahoo.com/table.csv?s=..."
 * 
 * @author Tom Boldan and Gal Schlezinger
 */
public class StockDay {
	private long date;
	private double open;
	private double close;
	private double low;
	private double high;
	private long volume;
	private double adjClose;
	private String symbol;

	public String getSymbol() {
		return symbol;
	}

	public StockDay setSymbol(String symbol) {
		this.symbol = symbol;

		return this;
	}

	public StockDay() {

	}

	public StockDay(StockDay stock) {
		this.setDate(stock.getDate()).setOpen(stock.getOpen()).setClose(stock.getClose()).setHigh(stock.getHigh())
				.setLow(stock.getLow()).setVolume(stock.getVolume()).setAdjClose(stock.getAdjClose())
				.setSymbol(stock.getSymbol());
	}

	public long getDate() {
		return date;
	}

	public StockDay setDate(long date) {
		this.date = date;

		return this;
	}

	public double getOpen() {
		return open;
	}

	public StockDay setOpen(double open) {
		this.open = open;

		return this;
	}

	public double getClose() {
		return close;
	}

	public StockDay setClose(double close) {
		this.close = close;

		return this;
	}

	public double getLow() {
		return low;
	}

	public StockDay setLow(double low) {
		this.low = low;

		return this;
	}

	public double getHigh() {
		return high;
	}

	public StockDay setHigh(double high) {
		this.high = high;

		return this;
	}

	public long getVolume() {
		return volume;
	}

	public StockDay setVolume(long volume) {
		this.volume = volume;

		return this;
	}

	public double getAdjClose() {
		return adjClose;
	}

	public StockDay setAdjClose(double adjClose) {
		this.adjClose = adjClose;

		return this;
	}

	/**
	 * This method parse a stock day from string
	 * 
	 * @param str
	 *            - the stock day string
	 * @return stock day
	 */
	public static StockDay parseStockDayFromSring(String str, String symbol) {
		String[] data = str.split(",");

		StockDay stockDay = new StockDay().setDate(LocalDate.parse(data[0]).toEpochDay())
				.setOpen(Double.parseDouble(data[1])).setHigh(Double.parseDouble(data[2]))
				.setLow(Double.parseDouble(data[3])).setClose(Double.parseDouble(data[4]))
				.setVolume(Long.parseLong(data[5])).setAdjClose(Double.parseDouble(data[6])).setSymbol(symbol);

		return stockDay;
	}

	/**
	 * Provides the string in the hadoop format file
	 * 
	 * @param features
	 *            - the analysis features
	 * @return {@link String} the string
	 */
	public String toString(AnalysisFeatures features) {
		StringBuilder strBuilder = new StringBuilder();

		String isOpen = features.isOpen() ? String.valueOf(open) : "@";
		String isClose = features.isClose() ? String.valueOf(close) : "@";
		String isLow = features.isLow() ? String.valueOf(low) : "@";
		String isHigh = features.isHigh() ? String.valueOf(high) : "@";

		strBuilder.append(this.getSymbol()).append(",").append(date).append(",").append(isOpen).append(",")
				.append(isHigh).append(",").append(isLow).append(",").append(isClose);

		return strBuilder.toString();
	}

	/**
	 * Normalize {@link StockDay}
	 * 
	 * @param min
	 * @param max
	 * @return a new and normalize {@link StockDay}
	 */
	public StockDay normalize(StockDay min, StockDay max) {
		StockDay stock = new StockDay();
		stock.setDate(this.getDate());
		stock.setOpen(finalproj.utils.MathUtils.normalizeData(this.getOpen(), min.getOpen(), max.getOpen(), 2));
		stock.setClose(finalproj.utils.MathUtils.normalizeData(this.getClose(), min.getClose(), max.getClose(), 2));
		stock.setHigh(finalproj.utils.MathUtils.normalizeData(this.getHigh(), min.getHigh(), max.getHigh(), 2));
		stock.setLow(finalproj.utils.MathUtils.normalizeData(this.getLow(), min.getLow(), max.getLow(), 2));
		stock.setSymbol(this.getSymbol());

		return stock;
	}
}
