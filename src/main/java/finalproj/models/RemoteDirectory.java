package finalproj.models;

/**
 * Represents a remote directory
 * 
 * @author Tom Boldan and Gal Schlezinger
 *
 */
public class RemoteDirectory {
	private String base;

	public String base() {
		return base;
	}

	public RemoteDirectory setBase(String base) {
		this.base = base;

		return this;
	}

	public RemoteDirectory(String base) {
		this.setBase(base);
	}

	/**
	 * 
	 * @return Data sub directory
	 */
	public String data() {
		return base + "/data";
	}

	/**
	 * 
	 * @return Output sub directory
	 */
	public String output() {
		return base + "/output";
	}

	/**
	 * 
	 * @return Sources sub directory
	 */
	public String src() {
		return base + "/src";
	}

	/**
	 * 
	 * @return Kmeans result path
	 */
	public String kmeansResult() {
		return this.output() + "/kmeans/part-r-00000";
	}
}
