package finalproj.models;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import finalproj.stocks.FileDownloader;

/**
 * Represents a stock symbol (company)
 * 
 * @author Tom Boldan and Gal Schlezinger
 */
public class StockSymbol {
	private String symbol;
	private String name;
	private List<StockDay> stockDays;

	public List<StockDay> getStockDays() {
		return stockDays;
	}

	public StockSymbol setStockDays(List<StockDay> stockDays) {
		this.stockDays = stockDays;

		return this;
	}

	public String getSymbol() {
		return symbol;
	}

	public StockSymbol setSymbol(String symbol) {
		this.symbol = symbol;

		return this;
	}

	public String getName() {
		return name;
	}

	public StockSymbol setName(String name) {
		this.name = name;

		return this;
	}

	/**
	 * This method sets and returns the stock days base on the analysis features
	 * 
	 * @param dayNum
	 *            - numbers of days to fetch backwards
	 * @return the stock days
	 * @throws IOException
	 */
	public List<StockDay> fetchStockDays(int dayNum) throws IOException {
		this.setStockDays(FileDownloader.getStockDays(dayNum, symbol));

		return this.getStockDays();
	}

	/**
	 * This method parse {@link StockSymbol} from string
	 * 
	 * @param str
	 *            - the stock symbol string
	 * @return {@link StockSymbol}
	 */
	public static StockSymbol parstStockSymbolFromString(String str) {
		String[] data = str.split("\\|");

		StockSymbol stockSymbol = new StockSymbol().setSymbol(data[0]).setName(data[1]);

		return stockSymbol;
	}

	/**
	 * Normalize a {@link StockSymbol}
	 * 
	 * @return new and normalized {@link StockSymbol}
	 */
	public StockSymbol normalize() {
		StockDayRange range = StockDayRange.create(this.getStockDays());
		StockSymbol normaSymbol = new StockSymbol().setName(this.getName()).setSymbol(this.getSymbol());

		return normaSymbol.setStockDays(this.getStockDays().stream()
				.map(x -> x.normalize(range.getMin(), range.getMax())).collect(Collectors.toList()));
	}

	/**
	 * String representation
	 * 
	 * @param features
	 *            to clustering
	 * @return String representation
	 */
	public String toString(AnalysisFeatures features) {
		return this.getStockDays().stream().map(x -> x.toString(features)).collect(Collectors.joining(";"));
	}
}
