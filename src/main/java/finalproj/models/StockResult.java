package finalproj.models;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Represent a response result for the client
 * 
 * @author Tom Boldan and Gal Schlezinger
 *
 */
public class StockResult {
	String symbol;
	List<Double> open;
	List<Double> close;
	List<Double> high;
	List<Double> low;
	List<String> date;

	public List<String> getDate() {
		return date;
	}

	public StockResult setDate(List<String> date) {
		this.date = date;

		return this;
	}

	public String getSymbol() {
		return symbol;
	}

	public StockResult setSymbol(String symbol) {
		this.symbol = symbol;

		return this;
	}

	public List<Double> getOpen() {
		return open;
	}

	public StockResult setOpen(List<Double> open) {
		this.open = open;

		return this;
	}

	public List<Double> getClose() {
		return close;
	}

	public StockResult setClose(List<Double> close) {
		this.close = close;

		return this;
	}

	public List<Double> getHigh() {
		return high;
	}

	public StockResult setHigh(List<Double> high) {
		this.high = high;

		return this;
	}

	public List<Double> getLow() {
		return low;
	}

	public StockResult setLow(List<Double> low) {
		this.low = low;

		return this;
	}

	/**
	 * Parse a list of {@link StockDay} to {@link StockResult}
	 * 
	 * @param data
	 *            stock days
	 * @return {@link StockResult}
	 */
	public static StockResult parse(List<StockDay> data) {
		StockResult result = new StockResult();

		List<Double> open = data.stream().map(StockDay::getOpen).collect(Collectors.toList());
		List<Double> close = data.stream().map(StockDay::getClose).collect(Collectors.toList());
		List<Double> high = data.stream().map(StockDay::getHigh).collect(Collectors.toList());
		List<Double> low = data.stream().map(StockDay::getLow).collect(Collectors.toList());
		List<String> date = data.stream().map(x -> LocalDate.ofEpochDay(x.getDate()).toString())
				.collect(Collectors.toList());

		return result.setOpen(open).setClose(close).setHigh(high).setLow(low).setSymbol(data.get(0).getSymbol())
				.setDate(date);
	}
}
