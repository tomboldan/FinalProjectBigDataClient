package finalproj.utils;

import java.util.logging.Logger;

/**
 * Exposes the consts in the env variable
 * 
 * @author Tom Boldan and Gal Schlezinger
 */
public class EnvUtils {
	private static Logger logger = Logger.getLogger(EnvUtils.class.getName());

	public static String user() {
		return get("FINAL_BIGDATA_USER");
	}

	public static String password() {
		return get("FINAL_BIGDATA_PASSWORD");
	}

	public static String host() {
		return get("FINAL_BIGDATA_HOST");
	}

	/**
	 * Gets the env variable
	 * 
	 * @param name
	 * @return
	 */
	private static String get(String name) {
		String value = System.getenv(name);

		if (value == null || value.isEmpty()) {
			String error = "The env var is missing: " + name;
			logger.severe(error);

			throw new RuntimeException(error);
		}

		return value;
	}
}
