package finalproj.utils;

/**
 * Math utilities
 * 
 * @author Tom Boldan and Gal Schlezinger
 *
 */
public class MathUtils {

	/**
	 * Min-max normalization:
	 * 
	 * <pre>
	 * new_v = (v - min) / (max - min) * (new_max - new_min) + new_min
	 * </pre>
	 * 
	 * where
	 * 
	 * <pre>
	 * new_max = 100
	 * </pre>
	 * 
	 * and
	 * 
	 * <pre>
	 * new_min = 0
	 * </pre>
	 * 
	 * so
	 * 
	 * <pre>
	 * new_v = (v - min) / (max - min) * 100
	 * </pre>
	 * 
	 * @param data
	 * @param min
	 * @param max
	 * @param precision
	 * @return normalized data
	 */
	public static double normalizeData(double data, double min, double max, int precision) {
		double minMaxNormlValue = (data - min) / (max - min) * 100;
		int doubler = (int) Math.pow(10, precision);

		return Math.round(minMaxNormlValue * doubler) / doubler;
	}

}
