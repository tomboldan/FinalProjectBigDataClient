import static spark.Spark.get;
import static spark.Spark.staticFileLocation;

import finalproj.controllers.HandleDataRoute;

/**
 * Main
 * 
 * @author Tom Boldan and Gal Schlezinger
 *
 */
public class Main {

	public static void main(String[] args) {
		staticFileLocation("/public");
		get("/start", new HandleDataRoute());
	}
}
