module.exports = {
  devtool: 'eval',
  entry: './lib/index.js',
  output: {
    filename: 'src/main/resources/public/dist/bundle.js'
  },
  module: {
    loaders: [{
      test: /lib.*\.js$/,
      loader: 'babel-loader'
    }]
  }
};
